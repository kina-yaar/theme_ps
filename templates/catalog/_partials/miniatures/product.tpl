{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 
{block name='product_miniature_item'}
{$desktopcol= 4}
{$tabletcol = 4}
<div class="ttr_article col-lg-{12/$desktopcol} col-md-{12/$desktopcol} col-sm-{12/$tabletcol} col-xs-12">
<div class="grid">
  <article class="product-miniature js-product-miniature ttr_post" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      {block name='product_thumbnail'}
        <a href="{$product.url}" class="thumbnail product-thumbnail product-image">
          <img
            src = "{$product.cover.bySize.home_default.url}"
            alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
            data-full-size-image-url = "{$product.cover.large.url}"
          >
        </a>
      {/block}
<div class="product-shop">

      <div class="product-description product-shop-margin postcontent">
        {block name='product_name'}
        <div class="ttr_post_inner_box">
          <h1 class="ttr_post_title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h1>
          </div>
        {/block}

        {block name='product_price_and_shipping'}
          {if $product.show_price}
            <div class="product-price-and-shipping">
              {if $product.has_discount}
                {hook h='displayProductPriceBlock' product=$product type="old_price"}

                <span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
                <span class="regular-price">{$product.regular_price}</span>
                {if $product.discount_type === 'percentage'}
                  <span class="discount-percentage">{$product.discount_percentage}</span>
                {/if}
              {/if}

              {hook h='displayProductPriceBlock' product=$product type="before_price"}

              <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
              <span itemprop="price  product-price" class="price">{$product.price}</span>

              {hook h='displayProductPriceBlock' product=$product type='unit_price'}

            {hook h='displayProductPriceBlock' product=$product type='weight'}
          </div>
        {/if}
      {/block}

      {block name='product_reviews'}
        {hook h='displayProductListReviews' product=$product}
      {/block}
   

    {block name='product_flags'}
      <ul class="product-flags">
        {foreach from=$product.flags item=flag}
          <li class="product-flag {$flag.type}">{$flag.label}</li>
        {/foreach}
      </ul>
    {/block}
    
    
     <div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
      {block name='quick_view'}
      <div class="add-to-links">
        <a class="quick-view" href="#" data-link-action="quickview">
          <!--<i class="material-icons search">&#xE8B6;</i> -->{l s='Quick view' d='Shop.Theme.Actions'}
        </a>
        </div>
      {/block}

      {block name='product_variants'}
        {if $product.main_variants}
          {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
        {/if}
      {/block}
    </div>
    
    {block name='add_to_cart'}
      <div class="add-to-cart">
        <form action="{$urls.pages.cart}" method="post">
			<input type="hidden" value="{$product.id_product}" name="id_product">
		<button  data-button-action="add-to-cart" class="btn btn-default">
		{l s='Add to cart' d='Shop.Theme.Actions'}
		</button>
		</form>
      </div>
    {/block}
     
   </div><!--product-shop-margin postcontent div close -->
    
    
    

    </div><!-- product-shop div close -->
  </article>
  </div> <!-- ttr_article div closed -->
</div><!-- bootestrap column div close-->
{/block}
