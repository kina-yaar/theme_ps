{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Merchandise returns' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}

  {if $ordersReturn && count($ordersReturn)}

    <h6 class="ttr_prodcheckboxcontent">{l s='Here is a list of pending merchandise returns' d='Shop.Theme.Customeraccount'}</h6>
<div class="block-center" id="block-history">
    <table id="order-list" class="table  table-bordered hidden-sm-down  ttr_prochec_table_background footab footable-loaded footable default">
      <thead class=" table_head_background">
        <tr class="ttr_prochec_Heading">
          <th>{l s='Order' d='Shop.Theme.Customeraccount'}</th>
          <th>{l s='Return' d='Shop.Theme.Customeraccount'}</th>
          <th>{l s='Package status' d='Shop.Theme.Customeraccount'}</th>
          <th>{l s='Date issued' d='Shop.Theme.Customeraccount'}</th>
          <th>{l s='Returns form' d='Shop.Theme.Customeraccount'}</th>
        </tr>
      </thead>
      <tbody>
        {foreach from=$ordersReturn item=return}
          <tr>
            <td class="ttr_prochec_product_description "><a href="{$return.details_url}">{$return.reference}</a></td>
            <td class="ttr_prochec_product_link"><a href="{$return.return_url}">{$return.return_number}</a></td>
            <td class="ttr_prochec_product_description">{$return.state_name}</td>
            <td class="ttr_prochec_product_description">{$return.return_date}</td>
            <td class="text-sm-center">
              {if $return.print_url}
                <a href="{$return.print_url}">{l s='Print out' d='Shop.Theme.Actions'}</a>
              {else}
                -
              {/if}
            </td>
          </tr>
        {/foreach}
      </tbody>
    </table>
</div>
    <div class="order-returns hidden-md-up">
      {foreach from=$ordersReturn item=return}
        <div class="order-return ttr_prochec_table_background">
          <ul>
            <li>
              <strong class="ttr_prochec_Heading">{l s='Order' d='Shop.Theme.Customeraccount'} : </strong>
            <span class="ttr_prochec_product_description ">  <a href="{$return.details_url}">{$return.reference}</a></span>
            </li>
            <li>
              <strong class="ttr_prochec_Heading">{l s='Return' d='Shop.Theme.Customeraccount'}:</strong>
           <span class="ttr_prochec_product_link">   <a href="{$return.return_url}">{$return.return_number}</a></span>
            </li>
            <li>
              <strong class="ttr_prochec_Heading">{l s='Package status' d='Shop.Theme.Customeraccount'} : </strong>
             <span class="ttr_prochec_product_description ">   {$return.state_name}</span>
            </li>
            <li>
              <strong class="ttr_prochec_Heading">{l s='Date issued' d='Shop.Theme.Customeraccount'} : </strong>
             <span class="ttr_prochec_product_description ">   {$return.return_date}</span>
            </li>
            {if $return.print_url}
              <li>
                <strong class="ttr_prochec_Heading">{l s='Returns form' d='Shop.Theme.Customeraccount'} : </strong>
                <a href="{$return.print_url}">{l s='Print out' d='Shop.Theme.Actions'}</a>
              </li>
            {/if}
          </ul>
        </div>
      {/foreach}
    </div>

  {/if}

{/block}
