{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Order details' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
<div id="block-order-detail">
  {block name='order_infos'}
    <div id="order-infos">
      <div class="box ttr_prochec_table_background ">
          <div class="row">
            <div class="ttr_order-align col-xs-{if $order.details.reorder_url}9{else}12{/if}">
              <strong class="ttr_prochec_product_description">
                {l
                  s='Order Reference %reference% - placed on %date%'
                  d='Shop.Theme.Customeraccount'
                  sprintf=['%reference%' => $order.details.reference, '%date%' => $order.details.order_date]
                }
              </strong>
            </div>
            {if $order.details.reorder_url}
              <div class="col-xs-3 text-xsright forgotpassword ">
                <a  href="{$order.details.reorder_url}" class="button-primary">{l s='Reorder' d='Shop.Theme.Actions'}</a>
              </div>
            {/if}
            <div style="clear: both;"></div>
          </div>
      </div>

      <div class="box ttr_prochec_table_background forgotpassword">
          <ul>
            <li class="ttr_prochec_product_description"><strong>{l s='Carrier' d='Shop.Theme.Checkout'}</strong> {$order.carrier.name}</li>
            <li class="ttr_prochec_product_description"><strong>{l s='Payment method' d='Shop.Theme.Checkout'}</strong> {$order.details.payment}</li>

            {if $order.details.invoice_url}
              <li>
                <a href="{$order.details.invoice_url}">
                  {l s='Download your invoice as a PDF file.' d='Shop.Theme.Customeraccount'}
                </a>
              </li>
            {/if}

            {if $order.details.recyclable}
              <li>
                {l s='You have given permission to receive your order in recycled packaging.' d='Shop.Theme.Customeraccount'}
              </li>
            {/if}

            {if $order.details.gift_message}
              <li>{l s='You have requested gift wrapping for this order.' d='Shop.Theme.Customeraccount'}</li>
              <li>{l s='Message' d='Shop.Theme.Customeraccount'} {$order.details.gift_message nofilter}</li>
            {/if}
          </ul>
      </div>
    </div>
  {/block}

  {block name='order_history'}
    <section id="order-history">
      <h3 class="ttr_prochec_product_title">{l s='Follow your order\'s status step-by-step' d='Shop.Theme.Customeraccount'}</h3>
      <table id="cart_summary" class="table detail_step_by_step  ttr_prochec_table_background table-bordered table-labeled hidden-xs-down">
        <thead class="table_head_background">
          <tr class="ttr_prochec_Heading">
            <th>{l s='Date' d='Shop.Theme.Global'}</th>
            <th>{l s='Status' d='Shop.Theme.Global'}</th>
          </tr>
        </thead>
        <tbody>
          {foreach from=$order.history item=state}
            <tr class="ttr_prochec_product_description">
              <td class="step-by-step-date">{$state.history_date}</td>
              <td>
                <span class="label label-pill {$state.contrast}" style="background-color:{$state.color}">
                  {$state.ostate_name}
                </span>
              </td>
            </tr>
          {/foreach}
        </tbody>
      </table>
      <div class="hidden-sm-up history-lines">
        {foreach from=$order.history item=state}
          <div class="history-line">
            <div class="date ttr_prochec_product_description">{$state.history_date}</div>
            <div class="state">
              <span class="label label-pill {$state.contrast}" style="background-color:{$state.color}">
                {$state.ostate_name}
              </span>
            </div>
          </div>
        {/foreach}
      </div>
    </section>
  {/block}

  {if $order.follow_up}
    <div class="box">
      <p>{l s='Click the following link to track the delivery of your order' d='Shop.Theme.Customeraccount'}</p>
      <a href="{$order.follow_up}">{$order.follow_up}</a>
    </div>
  {/if}

  {block name='addresses'}
    <div class="addresses">
      {if $order.addresses.delivery}
        <div class="col-lg-6 col-md-6 col-sm-6">
          <article id="delivery-address" class="box  ttr_shipping_background ttr_register ">
            <h3 class="ttr_prodaddressheading page-subheading">{l s='Delivery address %alias%' d='Shop.Theme.Checkout' sprintf=['%alias%' => $order.addresses.delivery.alias]}</h3>
            <address class="ttr_prodaddresscontent">{$order.addresses.delivery.formatted nofilter}</address>
          </article>
        </div>
      {/if}

      <div class="col-lg-6 col-md-6 col-sm-6">
        <article id="invoice-address" class="box ttr_shipping_background ttr_signin">
          <h3 class="ttr_prodaddressheading page-subheading">{l s='Invoice address %alias%' d='Shop.Theme.Checkout' sprintf=['%alias%' => $order.addresses.invoice.alias]}</h3>
          <address class="ttr_prodaddresscontent">{$order.addresses.invoice.formatted nofilter}</address>
        </article>
      </div>
      <div style="clear: both;"></div>
    </div>
  {/block}

  {$HOOK_DISPLAYORDERDETAIL nofilter}

  {block name='order_detail'}
    {if $order.details.is_returnable}
      {include file='customer/_partials/order-detail-return.tpl'}
    {else}
      {include file='customer/_partials/order-detail-no-return.tpl'}
    {/if}
  {/block}

  {block name='order_carriers'}
    {if $order.shipping}
      <div class="box track">
        <table id="cart_summary" class="table  footab table-bordered hidden-sm-down ttr_prochec_table_background">
          <thead class="table_head_background">
            <tr class="ttr_prochec_Heading">
              <th>{l s='Date' d='Shop.Theme.Global'}</th>
              <th>{l s='Carrier' d='Shop.Theme.Checkout'}</th>
              <th>{l s='Weight' d='Shop.Theme.Checkout'}</th>
              <th>{l s='Shipping cost' d='Shop.Theme.Checkout'}</th>
              <th>{l s='Tracking number' d='Shop.Theme.Checkout'}</th>
            </tr>
          </thead>
          <tbody>
            {foreach from=$order.shipping item=line}
              <tr class="ttr_prochec_product_description">
                <td>{$line.shipping_date}</td>
                <td>{$line.carrier_name}</td>
                <td>{$line.shipping_weight}</td>
                <td>{$line.shipping_cost}</td>
                <td>{$line.tracking}</td>
              </tr>
            {/foreach}
          </tbody>
        </table>
        <div class="hidden-md-up shipping-lines ttr_prochec_table_background">
          {foreach from=$order.shipping item=line}
            <div >
              <ul>
                <li>
                  <strong class="ttr_prochec_Heading">{l s='Date' d='Shop.Theme.Global'} : </strong> <span class="ttr_prochec_product_description">{$line.shipping_date}</span>
                </li>
                <li>
                  <strong class="shipping-line ttr_prochec_Heading">{l s='Carrier' d='Shop.Theme.Checkout'} : </strong>  <span class="ttr_prochec_product_description">{$line.carrier_name}</span>
                </li>
                <li>
                  <strong class="shipping-line ttr_prochec_Heading">{l s='Weight' d='Shop.Theme.Checkout'} : </strong>  <span class="ttr_prochec_product_description">{$line.shipping_weight}</span>
                </li>
                <li>
                  <strong class="shipping-line ttr_prochec_Heading">{l s='Shipping cost' d='Shop.Theme.Checkout'} : </strong>  <span class="ttr_prochec_product_description">{$line.shipping_cost}</span>
                </li>
                <li>
                  <strong class="shipping-line ttr_prochec_Heading">{l s='Tracking number' d='Shop.Theme.Checkout'} : </strong>  <span class="ttr_prochec_product_description">{$line.tracking}</span>
                </li>
              </ul>
            </div>
          {/foreach}
        </div>
      </div>
    {/if}
  {/block}

  {block name='order_messages'}
    {include file='customer/_partials/order-messages.tpl'}
  {/block}
{/block}
</div>
