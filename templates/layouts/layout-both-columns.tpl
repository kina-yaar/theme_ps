<!doctype html>
<html lang="{$language.iso_code}">
<head>
{block name="head"}
{include file="_partials/head.tpl"}
{/block}
</head>
{if $page.page_name == "product"}
{assign var="page_name" value=productdescription}
{assign var="tt_page_name" value=ttr_ecommerce}
{elseif $page.page_name ==" order" || $page.page_name  == "module-bankwire-payment" || $page.page_name  == "module-cheque-payment" || $page.page_name  == "order-confirmation" || $page.page_name  == "authentication" || $page.page_name  == "contact" || $page.page_name  == "history" || $page.page_name  == "addresses" || $page.page_name  == "identity"  || $page.page_name  == "module-blockwishlist-mywishlist" || $page.page_name  == "my-account" || $page.page_name == "order-detail" || $page.page_name == "address" || $page.page_name == "order-slip" || $page.page_name  == "cart" || $page.page_name  == "stores" || $page.page_name == 'order-follow'}
{assign var="page_name" value=productcheckout}
{assign var="tt_page_name" value=ttr_ecommerce}
{else }
{assign var="page_name" value=""}
{assign var="tt_page_name" value=""}
{/if}
{if $page.page_name == "index"}
{assign var="tt_page_name" value=ttr_ecommerce}
{/if}
<body id="{$page.page_name}" class="{$page.body_classes|classnames} {$page_name} {$tt_page_name}">
{block name="hook_after_body_opening_tag"}
{hook h="displayAfterBodyOpeningTag"}
{/block}
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
 <main id="ttr_page" class="container">
 {block name="product_activation"}
{include file="catalog/_partials/product-activation.tpl"}
 {/block}
{block name="header"}
  {include file="_partials/header.tpl"}
{/block}
{block name="header"}
  {include file="_partials/slideshow.tpl"}
  {/block}
{block name="header"}
  {include file="_partials/menu.tpl"}
  {/block}
 {block name="notifications"}
 {include file="_partials/notifications.tpl"}
{/block}
<div class="ttr_content_and_sidebar_container">
 <section id="wrapper">
{hook h="displayWrapperTop"}
<div>
 {block name="left_column"}
<div id="ttr_sidebar_left">
  <div id="ttr_sidebar_left_margin">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_sidebar_left_padding">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
   {if $page.page_name == "product"}
   {hook h="displayLeftColumnProduct"}
{else}
  {hook h="displayLeftColumn"}
 {/if}
</div>
 </div>
 </div>
{/block}
 {block name="content_wrapper"}
 <div id="ttr_content_both_sidebar">
 <div id="ttr_content">
 <div id="ttr_content_margin" class="container-fluid">
{if $page.page_name != "index"}
 {block name="breadcrumb"}
 {include file="_partials/breadcrumb.tpl"}
 {/block}
{/if}
 <div id="content-wrapper">
 {hook h="displayContentWrapperTop"}
 {block name="content"}
  <p>Hello world! This is HTML5 Boilerplate.</p>
 {/block}
 {hook h="displayContentWrapperBottom"}
 </div>
 </div>
 </div>
 </div>
{/block}
 {block name="right_column"}
<div id="ttr_sidebar_right">
<div id="ttr_sidebar_right_margin">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_sidebar_right_padding">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
{if $page.page_name == "product"}
 {hook h="displayRightColumnProduct"}
 {else}
 {hook h="displayRightColumn"}
{/if}
</div>
 </div>
 </div>
 {/block}
</div>
{hook h="displayWrapperBottom"}
</section>
</div>
<div style="clear: both;"></div>
 <footer id="ttr_footer">
 {block name="footer"}
{include file="_partials/footer.tpl"}
 {/block}
 </footer>
 </main>
 {block name="javascript_bottom"}
 {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
<script type="text/javascript"src="{$urls.js_url}customjs.js" ></script>
<script type="text/javascript" src="{$urls.js_url}html5shiv.js" ></script>
<script type="text/javascript" src="{$urls.js_url}respond.min.js" ></script>
<script type="text/javascript" src="{$urls.js_url}tt_slideshow.js" ></script>
<script type="text/javascript"src="{$urls.js_url}totop.js" ></script>
 {/block}
 {block name="hook_before_body_closing_tag"}
 {hook h="displayBeforeBodyClosingTag"}
  {/block}
</body>
</html>
