<div class="totopshow">
<a href="#" class="back-to-top"><img alt="Back to Top" src="{$urls.img_url}../images/gototop.png"/></a>
</div>
{block name="header_top"}
<header id="ttr_header">
<div id="ttr_header_inner" class="ttr_header_innerPS">
<div class="ttr_header_logo">
<a id="header_logo" href="{$urls.base_url}" title="{$shop.name}" target="_self"> 
<img src="{$shop.logo}" alt="{$shop.name}" />
</a>
</div>
{block name="header_nav"}
<!-- start header links-->
<div class="ttr_header_links  hidden-xs-down">
<div class="right-nav hidden-xs-down">
 {hook h="displayNav1"}
 {hook h="displayNav2"}
</div>
</div>
<div class="ttr_header_links  ttr_header_linksPS  hidden-md-up">
<div class="right-nav hidden-md-up">
 {hook h="displayNav1"}
 {hook h="displayNav2"}
 <div id="_mobile_language_selector" class="link hidden-md-up"></div>
 <div id="_mobile_contact_link" class="link hidden-md-up"></div>
</div>
</div>
<!-- end header links-->
{/block}
<a class="ttr_header_rss" href="#">
</a>
<a href="http://www.facebook.com/TemplateToaster" class="ttr_header_facebook"  target="_self" >
</a>
<a href="http://www.templatetoaster.com" class="ttr_header_linkedin" target="_self" >
</a>
<a href="http://twitter.com/templatetoaster" class="ttr_header_twitter" target="_self" >
</a>
<a href="http://www.templatetoaster.com" class="ttr_header_googleplus" target="_self" >
</a>
<!-- start serach bar-->
<div id="search_block_top"class="  input-group">
{hook h="displaySearch"}
</div>
<!-- end serach bar-->
</div>
</header>
{/block}