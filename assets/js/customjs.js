jQuery(document).ready(function(){
checkbox();
radiobutton();
$(document).ajaxComplete(function() { checkbox();});
$(document).ajaxComplete(function() { radiobutton();});

/*************** Staticfooter script ***************/
var window_height =  Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var body_height = jQuery(document.body).height();
var content = jQuery("div[id$='content_margin']");
if(body_height < window_height){
differ = (window_height - body_height);
content_height = content.height() + differ;
jQuery("div[id$='content_margin']").css({"min-height":content_height+"px"});
}

/* Slideshow Function Call */

if(jQuery('#ttr_slideshow_inner').length){
jQuery('#ttr_slideshow_inner').TTSlider({
stopTransition:false,slideShowSpeed:4000, begintime:1000,cssPrefix: 'ttr_'
});
}

/*************** Hamburgermenu slideleft script ***************/
jQuery('#nav-expander').on('click',function(e){
e.preventDefault();
jQuery('body').toggleClass('nav-expanded');
});

/*************** Menu click script ***************/
jQuery('ul.ttr_menu_items.nav li [data-toggle=dropdown]').on('click', function() {
var window_width =  Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
if(window_width > 1025 && jQuery(this).attr('href')){
window.location.href = jQuery(this).attr('href'); 
}
else{
if(jQuery(this).parent().hasClass('open')){
location.assign(jQuery(this).attr('href'));
}
}
});

/*************** Sidebarmenu click script ***************/
jQuery('ul.ttr_vmenu_items.nav li [data-toggle=dropdown]').on('click', function() {
var window_width =  Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
if(window_width > 1025 && jQuery(this).attr('href')){
window.location.href = jQuery(this).attr('href'); 
}
else{
if(jQuery(this).parent().hasClass('open')){
location.assign(jQuery(this).attr('href'));
}
}
});

/*************** Tab menu click script ***************/
jQuery('.ttr_menu_items ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) { 
var window_width =  Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if(window_width < 1025){
event.preventDefault();
event.stopPropagation();
jQuery(this).parent().siblings().removeClass('open');
jQuery(this).parent().toggleClass(function() {
if (jQuery(this).is(".open") ) {
window.location.href = jQuery(this).children("[data-toggle=dropdown]").attr('href'); 
return "";
} else {
return "open";
}
});
}
});

/*************** Tab-Sidebarmenu script ***************/
jQuery('.ttr_vmenu_items ul [data-toggle=dropdown]').on('click', function(event) { 
var window_width =  Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if(window_width < 1025){
event.preventDefault();
event.stopPropagation();
jQuery(this).parent().siblings().removeClass('open');
jQuery(this).parent().toggleClass(function() {
if (jQuery(this).is(".open") ) {
window.location.href = jQuery(this).children("[data-toggle=dropdown]").attr('href'); 
return "";
} else {
return "open";
}
});
}
});

/*************** Cart Block visibilty ***************/
if(jQuery('.ttr_header_cart').length){
jQuery('.ttr_header_cart').mouseover(function() {
jQuery('.ttr_header_cart .cart_block.exclusive').css('display','block');
 });
jQuery('.ttr_header_cart').mouseout(function() {
jQuery('.ttr_header_cart .cart_block.exclusive').css('display','none');
}); 
}
});

/*****************PERSONALISATION********************/

var UID = {
	_current: 0,
	getNew: function(){
		this._current++;
		return this._current;
	}
};

HTMLElement.prototype.pseudoStyle = function(element,prop,value){
	var _this = this;
	var _sheetId = "pseudoStyles";
	var _head = document.head || document.getElementsByTagName('head')[0];
	var _sheet = document.getElementById(_sheetId) || document.createElement('style');
	_sheet.id = _sheetId;
	var className = "pseudoStyle" + UID.getNew();
	
	_this.className +=  " "+className; 
	
	_sheet.innerHTML += " ."+className+":"+element+"{"+prop+":"+value+"}";
	_head.appendChild(_sheet);
	return this;
};

/*-----------------------*/
document.getElementById("togglerP").addEventListener("mouseover", mouseOver);
document.getElementById("togglerP").addEventListener("mouseout", mouseOut);

function mouseOver() {
  if(document.getElementById('menuG').style.visibility=="visible"){
    document.getElementById("togglerP").style.transform = "rotate(225deg)";
    document.getElementById("ham").style.transform = "rotate(225deg)";
  }
}

function mouseOut() {
  if(document.getElementById('menuG').style.visibility=="visible"){
    document.getElementById("togglerP").style.transform = "rotate(135deg)";
    document.getElementById("ham").style.transform = "rotate(135deg)";
  }
}

    

function voir(){
    var toggler = document.getElementById('togglerP');
    var divham = document.getElementById('ham');
    var menu = document.getElementById('menuG');
    var divm1 = document.getElementById('m1');
    var divm2 = document.getElementById('m2');

    if(menu.style.visibility=="visible"){
      /*1*/divham.style.transform ="rotate(0deg)";
      /*2*/divham.pseudoStyle("before","top","-10px !important").pseudoStyle("before","transform","rotate(0deg) !important");
      /*3*/divham.pseudoStyle("after","top","10px !important").pseudoStyle("after","transform","rotate(0deg) !important");
      /*4*/menu.style.visibility="hidden";
      
      /*5*/divm1.style.transform = "scale(0)";
      /*6*/divm2.style.opacity = "0";
      /*7*/divm2.style.transition = "opacity 0.4s ease";
      /*7*/divm1.style.transition = "all 0.4s ease;";


      console.log(menu.style.visibility);
    }else{

        /* Toggler Animation */
        /*1*/divham.style.transform ="rotate(135deg)"

        /* Turns Lines Into X */
        /*2*/divham.pseudoStyle("before","top","0 !important").pseudoStyle("before","transform","rotate(90deg) !important");
        /*3*/divham.pseudoStyle("after","top","0 !important").pseudoStyle("after","transform","rotate(90deg) !important");

        /* Show Menu */
        /*4*/menu.style.visibility = "visible";
        /*5*/divm1.style.transform = "scale(1)";
        /*6*/divm2.style.opacity = "1";
        /*7*/divm2.style.transition = "opacity 0.4s ease 0.4s";
        /*8*/divm1.style.transitionDuration = "var(--menu-speed)";
        console.log(menu.style.visibility);
        } 
      }

      /*************************Search Box*****************************/
      $(".searchbtn").click(
      	function(){
            //$(this).removeClass("voir_boxSearch");
            //$(this).toggleClass("cache_boxSearch");
            $(this).toggleClass("bg-green");
            //$(".searchbtn_click").removeClass("cache_boxSearch");
            $(".searchbtn_click").toggleClass("bg-green voir_boxSearch");
            $(".fas-t").toggleClass("voir_boxSearch");
            $(".fas-r").toggleClass("cache_boxSearch");
            $(".fas").toggleClass("color-white");
            //$(".input").focus().removeClass("visi-n").val('');
            $(".input").focus().toggleClass("visi-o").val('');
            $(".input").focus().toggleClass("active-width").val('');
            $(".inputPS").focus().toggleClass("active-widthPS").val('');

            /*function controle(){
				var t = document.formP.s.value;
				//console.log(document.formP.s.value);
				if(t == "") {
				   alert('Le champ ne peut pas rester vide !! Veuillez introduire votre formule de calcul');
		 
		 
				  }
				  else {

            		document.getElementById("myLabelP").htmlFor = "bp";
					//document.form.method = "get";
					//document.form.action = "res1.php";
					//document.form.submit();
				  } 
				}*/

            
            });

      /********************BP_SHOPPING-CART**********/
      function bp_cart(){
    var myLink = document.getElementById('monLienPS');
    window.location = myLink.href;
}