{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!-- Block search module TOP -->

<div id="search_widget" class=" input-group form-search search-widget" data-search-controller-url="{$search_controller_url}">
	<form id="searchbox" method="get" action="{$search_controller_url}" name="formP">
		<p>

      <div id="box_desk">
        <div class="wrapperPS">
          <div class="searchbox">
            <input type="hidden" name="controller" value="search">
            <input style="visibility: hidden;" id="search_query_top" class="form-control input" type="text" name="s" value="{$search_string}" placeholder="{l s='Search our catalog' d='Shop.Theme.Catalog'}" aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
                  
            <div class="searchbtn_click cache_boxSearch" style="">
              <span class="input-group-btn" style="left: 52%;top: 40%;height: auto;">
                <button id="bp" claas="bp" name="submit_search" value="Search" class="ttr_button btn btn-default" type="submit">
                  <i class="fas fa fa-search"></i>
                </button>
                
              </span>           
            </div>

            <div class="searchbtn">
              <label id="myLabelP">
                <i class="fas-t fas fa fa-times cache_boxSearch"></i>  
                <i class="fas-r fas fa fa-search"></i>                     
              </label>
            </div>
                
          </div>
        </div>
      </div>
      <!--************************-->

      <div id="box_mob">
      <div class="perso">
        <div class="searchbtn">

          </div><div class="searchbtn_click cache_boxSearch" style="">
              <span class="input-group-btn" style="left: 52%;top: 40%;height: auto;">
                <button id="bp" claas="bp" name="submit_search" value="Search" class="ttr_button btn btn-default" type="submit">
                  <i class="fas fa fa-search"></i>
                </button>
                
              </span>           
            </div>

            <div class="searchbtn">
              <label id="myLabelP">
                <i class="fas-t fas fa fa-times cache_boxSearch"></i>  
                <i class="fas-r fas fa fa-search"></i>                     
              </label>
            </div>
      </div>
        
        <div class="wrapperPS">
          <div class="searchbox">
            <input type="hidden" name="controller" value="search">
            <input style="visibility: hidden;" id="search_query_top" class="form-control inputPS input" type="text" name="s" value="{$search_string}" placeholder="{l s='Search our catalog' d='Shop.Theme.Catalog'}" aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
          </div>
        </div>
      </div>
		</p>
	</form>
</div>